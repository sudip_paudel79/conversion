USAGE:

require_relative 'convert_phone_number_to_word'

phone_number = '2282698687'
converter = ConvertPhoneNumberToWord.new
converter.convert(phone_number)

10 digit number('2282698687') is given by the user, all possible words or combination of words from the provided dictionary is returned. The phone number mapping to letters is as follows:

2 = a b c

3 = d e f

4 = g h i

5 = j k l

6 = m n o

7 = p q r s

8 = t u v

9 = w x y z

2282668687 returns the following list [["act", "amounts"], ["act", "contour"], ["acta", "mounts"], ["bat", "amounts"], ["bat", "contour"], ["cat", "contour"], "catamounts"]


BENCHMARKING OF CODE

require 'benchmark'

Benchmark.measure do
  require_relative 'convert_phone_number_to_word'
  converter = ConvertPhoneNumberToWord.new
  converter.convert('2282698687')
end
=> #<Benchmark::Tms:0x00007f970aea9dd8 @label="", @real=0.7793579995632172, @cstime=0.0, @cutime=0.0, @stime=0.055229, @utime=0.6596270000000001, @total=0.714856>
