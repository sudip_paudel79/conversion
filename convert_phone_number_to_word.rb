# frozen_string_literal: true

require 'byebug'
class ConvertPhoneNumberToWord
  DIGIT_TO_CHAR_MAPPER = {
    '2' => %w[A B C],
    '3' => %w[D E F],
    '4' => %w[G H I],
    '5' => %w[J K L],
    '6' => %w[M N O],
    '7' => %w[P Q R S],
    '8' => %w[T U V],
    '9' => %w[W X Y Z]
  }.freeze

  INVALID_PHONE_NUMBER_ERROR = 'Given number is not valid number'

  def convert(phone_number)
    @phone_number = phone_number
    return INVALID_PHONE_NUMBER_ERROR unless phone_number_valid?

    @correct_words = []
    @phone_keys = nil
    extract_words
    three_combinations
  end

  private

  # validates if the phone_number exists and phone number is of length 10 digits with number from 2 to 9 only
  def phone_number_valid?
    matched_number = @phone_number.match(/^[2-9]*$/)
    @phone_number && matched_number && @phone_number.length == 10
  end

  # extracted words from dictionary
  def dictionary_words
    @dictionary_words ||= File.read('dictionary.txt').split("\n")
  end

  # convert each digit of phone number to array character(eg:- 2=> ['A','B','C'])
  def phone_keys
    @phone_keys ||= @phone_number.chars.map { |n| DIGIT_TO_CHAR_MAPPER[n] }
  end

  # extracting words
  def extract_words
    length = @phone_number.length
    i = 2
    # converting phone number to two combination of array chars
    while i < (length - 3)
      phone_chars1 = phone_keys[0..i]
      phone_chars2 = phone_keys[(i + 1)..(length - 1)]
      get_correct_words([phone_chars1, phone_chars2])
      i += 1
    end
  end

  # get the correct words after matching to the dictionary words
  def get_correct_words(phone_chars_array)
    matches = []
    phone_chars_array.each do |phone_chars|
      possible_words = phone_chars.shift.product(*phone_chars).map(&:join) # makes a possible words from one of the two combinations
      possible_words.reject! { |x| x.length < 3 } # if word is less than 3 characters reject it
      matches << (possible_words & dictionary_words) # matching the unrejected words with dictionary word and pushing it in matches variable
    end
    return if matches.any?(&:empty?) # if any of the array in matches is empty then return

    if matches.size == 2 # For two combinations of phone number combining all possible outcomes
      @correct_words += matches[0].product(matches[1])
    elsif matches.size == 3 # For three combination of phone number comnining all possible outcomes
      @correct_words += matches[0].product(matches[1]).product(matches[2]).map(&:flatten)
    end
  end

  # extracting mininum three digit phone number that is divided into three parts to make 10 digit phone number
  def three_combinations
    get_correct_words([phone_keys[0..2], phone_keys[3..5], phone_keys[6..9]])
    get_correct_words([phone_keys[0..2], phone_keys[3..6], phone_keys[7..9]])
    get_correct_words([phone_keys[0..3], phone_keys[4..6], phone_keys[7..9]])
    @correct_words << (phone_keys.shift.product(*phone_keys).map(&:join) & dictionary_words).join(', ') # add more words to variable @correct_words
    @correct_words.reject(&:empty?) # only select those words that is not empty string
  end
end
